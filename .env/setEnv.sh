#! /bin/bash

# Configure AWS CLI
function set_aws()
{
    echo "[default]" > /root/.aws/credentials
    echo "aws_access_key_id = ${ACC_KEY}" >> /root/.aws/credentials
    echo "aws_secret_access_key = ${ACC_SCT}" >> /root/.aws/credentials
    echo "[default]" > /root/.aws/config
    echo "region = ${_REGION}" >> /root/.aws/config
    echo "output = ${OUTPUT}" >> /root/.aws/config
}

# Populate variables and initialize terraform
function set_tf()
{
    chmod +x setTfEnv.sh
    /bin/bash setTfEnv.sh
    terraform init
}

# Set helm environment
function set_helm()
{
    . /root/.bash_completion
    source <(helm completion bash)
}

# Connect to EKS cluster
function set_cluster()
{
    aws eks --region ${_REGION} update-kubeconfig --name ${CLUSTER}
}

# Prepare terraform plan
function tf_plan()
{
    set_aws
    cd terraform
    set_tf
    terraform plan
    cd -
}

# Provision resources using terraform
function tf_apply()
{
    set_aws
    cd terraform
    set_tf
    terraform apply -auto-approve
    cd -
}

# Destroy resources using terraform
function tf_destroy()
{
    set_aws
    cd terraform
    set_tf
    terraform destroy -auto-approve
    cd -
}

# Configure kubectl and apply k8s dashboard
function set_kube()
{
    set_aws
    set_cluster
    cd /opt
    wget -O v0.3.6.tar.gz https://codeload.github.com/kubernetes-sigs/metrics-server/tar.gz/v0.3.6 && tar -xzf v0.3.6.tar.gz
    kubectl apply -f metrics-server-0.3.6/deploy/1.8+/
    cd -
    kubectl apply -f https://raw.githubusercontent.com/kubernetes/dashboard/v2.0.0-beta8/aio/deploy/recommended.yaml
    kubectl proxy &
    kubectl apply -f deployments/kubernetes-dashboard-admin.rbac.yaml
    kubectl -n kube-system describe secret $(kubectl -n kube-system get secret | grep service-controller-token | awk '{print $1}')
}

# Deploy Prometheus
function deploy_prometheus()
{
    kubectl create namespace prometheus
    helm install prometheus stable/prometheus --namespace prometheus --set alertmanager.persistentVolume.storageClass="gp2" --set server.persistentVolume.storageClass="gp2"
    sleep 60
    kubectl get all -n prometheus
}

# Expose Prometheus as LoadBalancer
function expose_prometheus()
{
    set_aws
    set_cluster
    kubectl get svc prometheus-server -n prometheus -o yaml > pms.yaml
    sed -i 's#type:\ ClusterIP#type:\ LoadBalancer#g' pms.yaml
    cat pms.yaml
    kubectl replace -f pms.yaml -n prometheus
    sleep 30
    export PLB=$(kubectl get svc -n prometheus prometheus-server -o jsonpath='{.status.loadBalancer.ingress[0].hostname}')
    echo "PROMETHEUS URL = http://${PLB}"
}

# Deploy Grafana
function deploy_grafana()
{
    sed -i "s#\";REGION;\"#\"${_REGION}\"#g" deployments/grafana.yaml
    sed -i "s#\";ACC_KEY;\"#\"${ACC_KEY}\"#g" deployments/grafana.yaml
    sed -i "s#\";ACC_SCT;\"#\"${ACC_SCT}\"#g" deployments/grafana.yaml
    kubectl create namespace grafana
    helm install grafana stable/grafana --namespace grafana --set persistence.storageClassName="gp2" --set persistence.enabled=true --set adminPassword="${GF_PWD}" --values deployments/grafana.yaml --set service.type=LoadBalancer
    sleep 30
    kubectl get all -n grafana
    export ELB=$(kubectl get svc -n grafana grafana -o jsonpath='{.status.loadBalancer.ingress[0].hostname}')
    echo "GRAFANA URL = http://${ELB}"
}

# Deploy monitoring stack
function deploy_monitoring()
{
    set_aws
    set_cluster
    set_helm
    deploy_prometheus
    deploy_grafana
}

# Remove Prometheus deployment
function remove_prometheus()
{
    helm uninstall prometheus --namespace prometheus
} 

# Remove Grafana deployment
function remove_grafana()
{
    helm uninstall grafana --namespace grafana
}

# Purge all deployments
function purge_deployments()
{
    set_aws
    set_cluster
    set_helm
    remove_grafana
    remove_prometheus
}
