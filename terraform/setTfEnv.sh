#! /bin/bash

echo -e "cluster_name=\"${CLUSTER}\"
\nregion=\"${_REGION}\"
\nvpc_name=\"${VPCNAME}\"
\ncidr_range=\"${CIDR_R}\"
\npv_sn1=\"${PV_SN1}\"
\npv_sn2=\"${PV_SN2}\"
\npv_sn3=\"${PV_SN3}\"
\npb_sn1=\"${PB_SN1}\"
\npb_sn2=\"${PB_SN2}\"
\npb_sn3=\"${PB_SN3}\"
\nenv_name=\"${ENVNAME}\"
\ngh_repo=\"${GHREPO}\"
\ngh_org=\"${GH_ORG}\"
" > terraform.tfvars