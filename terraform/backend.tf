# AWS S3 bucket where cluster tfstate is stored
# Must be created previous to pipeline execution
terraform {
  backend "s3" {
    bucket = "monitoring-s3-bucket"
    key    = "terraform/state/prod"
    region = "us-east-2"
  }
}