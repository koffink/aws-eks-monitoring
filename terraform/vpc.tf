provider "aws" {
  version = ">= 2.28.1"
  region  = "us-east-2"
}

data "aws_availability_zones" "available" {}

module "vpc" {
  source  = "terraform-aws-modules/vpc/aws"
  version = "2.6.0"

  name                 = "${var.vpc_name}"
  cidr                 = "${var.cidr_range}"
  azs                  = data.aws_availability_zones.available.names
  private_subnets      = ["${var.pv_sn1}", "${var.pv_sn2}", "${var.pv_sn3}"]
  public_subnets       = ["${var.pb_sn1}", "${var.pb_sn2}", "${var.pb_sn3}"]
  enable_nat_gateway   = true
  single_nat_gateway   = true
  enable_dns_hostnames = true

  tags = {
    "kubernetes.io/cluster/${var.cluster_name}" = "shared"
  }

  public_subnet_tags = {
    "kubernetes.io/cluster/${var.cluster_name}" = "shared"
    "kubernetes.io/role/elb"                      = "1"
  }

  private_subnet_tags = {
    "kubernetes.io/cluster/${var.cluster_name}" = "shared"
    "kubernetes.io/role/internal-elb"             = "1"
  }
}