# aws-eks-monitoring

Monitoring stack deployment on AWS using Kubernetes and Terraform

## Description
This pipeline will create a Kubernetes cluster on AWS, deploy prometheus on top of it, then deploy grafana, import Prometheus and Cloudwatch datasources and also import a few dashboards. Cleanup jobs must be ran manually.

### GitLab environment structure
* Directories
    * ./
        * .gitlab-ci.yml - Gitlab CI structure
        * README.md - This descriptor
    * ./.env
        * setEnv.sh - Library with all functions used by Gitlab CI
    * ./.sample
        * pipeline.png - Pipeline workflow image
    * ./deployments
        * grafana.yaml - Grafana datasources and dashboards config
        * kubernetes-dashboard-admin.rbac.yaml - Kubernetes dashboard configuration
    * ./docker
        * Dockerfile - File to build the docker imaged used by this project
    * ./terraform
        * backend.tf - AWS S3 bucket where cluster state file is stored
        * eks-cluster.tf - Create all the resources (AutoScaling Groups, etc...) required to set up an EKS cluster in the private subnets and bastion servers to access the cluster using the AWS EKS Module
        * kubernetes.tf - Define the kubernetes provider
        * outputs.tf - Define the output configuration
        * security-groups.tf - Create the security groups used by the EKS cluster
        * setTfEnv.sh - Populate terraform variables with content from Gitlab environment variables
        * variables.tf - Initialize terraform variables
        * versions.tf - Set terraform and other providers version for this project
        * vpc.tf - Create a VPC, subnets and availability zones using the AWS VPC Module
* Variables
    * _REGION - AWS CLI region config
    * ACC_KEY - AWS account access key ID
    * ACC_SCT - AWS account secret access key
    * CIDR_R  - VPC IP range
    * CLUSTER - EKS cluster name
    * DKRIMG  - Docker image (to be fetched from DockerHub)
    * ENVNAME - Environment name
    * GF_PWD  - Grafana admin password
    * GH_ORG  - GitHub project for terraform AWS modules
    * GHREPO  - GitHub project for terraform AWS EKS modules
    * OUTPUT  - AWS CLI output config
    * PB_SN1  - Public subnet 1 IP range
    * PB_SN2  - Public subnet 2 IP range
    * PB_SN3  - Public subnet 3 IP range
    * PV_SN1  - Private subnet 1 IP range
    * PV_SN2  - Private subnet 2 IP range
    * PV_SN3  - Private subnet 3 IP range
    * SETENV  - Path to setEnv script library
    * VPCNAME - VPC name

## Requirements
The pipeline expects the environment to be previously configured as follows

### Pre-requisites
* A valid AWS account
* An existing s3 bucket (for usage of remote cluster tfstate)
* GitLab environment properly configured (Environment variables, .tf files updated with hardcoded values - e.g. backend.tf)
* Configured AWS CLI
* AWS IAM authenticator
* Configured kubectl
* Configured terraform
* Configured helm

### AWS resources
* AWS account with IAM permission to manipulate resources
* S3 bucket
* Configuration to save tfstate file on s3 bucket __(backend.tf)__

### Docker
* Dockerfile is provided to build the image used by this project
* Docker image contains unzip, groff, less, curl, wget, git, awscli, kubectl, terraform and helm

## WorkFlow
* Pipeline Stages
    * prepare - Create terraform provisioning plan
        * prepare_plan
    * provision - Provision resources at AWS based on plan
        * apply_plan
    * deploy - Configure kubectl and deploy monitoring stack
        * deploy_kube
        * deploy_monitoring
    * expose - Expose Prometheus and k8s dashboard
        * expose_prometheus
    * purge - Remove monitoring stack deployments
        * purge_deployments
    * destroy - Destroy resources at AWS
        * destroy_plan

![Pipeline example](.sample/pipeline.png)
